import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FileuploadController } from './fileupload/fileupload.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { FileuploadService } from './fileupload/fileupload.service';
import { Products } from "./model/Products";
import { ProductsService } from "./model/ProductsService";
import { ProductsController } from './productsamount/productsController';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: '127.0.0.1',
      port: 5432,
      username: 'postgres',
      password: 'postgrespassword',
      database: 'postgres',
      synchronize: true,
      entities: [Products],
    }),
    TypeOrmModule.forFeature([Products])
  ],
  controllers: [AppController, FileuploadController, ProductsController],
  exports: [TypeOrmModule],
  providers: [AppService, FileuploadService, ProductsService],
})
export class AppModule {}
