import { Controller, Post, UploadedFile, UseInterceptors } from "@nestjs/common";
import { FileInterceptor } from '@nestjs/platform-express';
import { ProductsService } from "../model/ProductsService";
import { ProductDTO } from "../model/ProductDTO";
import { FileUploadResult } from "./FileUploadResult";

@Controller('/fileupload')
export class FileuploadController {

  constructor(
    private productsService: ProductsService
  ){}

  @Post()
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(@UploadedFile() file: Express.Multer.File): Promise<FileUploadResult> {
    console.log('fileupload::start::' + new Date().getTime());
    let read = JSON.parse(file.buffer.toString());
    let dtos: ProductDTO[] = new Array();
    Object.values(read).forEach(item => {
      let product: ProductDTO = {
        identifier: item["isin"],
        details: item
      };
      dtos.push(product);
    })
    dtos.forEach(item => {
      this.productsService.updateProduct(item);
    })
    console.log('fileupload::stop::' + new Date().getTime());
    let result: FileUploadResult = {
      code: "done"
    }
    return result;
  }
}
