import { Controller, Post } from "@nestjs/common";
import { ProductsService } from "../model/ProductsService";
import { ProductsAmountResult } from "./ProductsAmountResult";

@Controller()
export class ProductsController {

  constructor(
    private productsService: ProductsService
  ){}

  @Post("productsAmount")
  async getProductsAmount(): Promise<ProductsAmountResult> {
    let amount = await this.productsService.getProductsAmount();
    let result: ProductsAmountResult = {
      amount
    }
    return result;
  }
}
