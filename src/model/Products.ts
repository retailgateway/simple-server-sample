import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Products {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true, unique: true })
  identifier: string;

  @Column({ type: 'jsonb', nullable: true })
  details: Object;
}
