export interface ProductDTO {
  identifier: string;
  details: Object;
}
