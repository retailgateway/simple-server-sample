import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Products } from "./Products";
import { Repository } from "typeorm";
import { ProductDTO } from "./ProductDTO";

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Products) private productsRepository: Repository<Products>
  ) {}

  async getProductsAmount(): Promise<number> {
    return await this.productsRepository.count();
  }

  async updateProduct(produkt: ProductDTO): Promise<Products> {
    let result = await this.productsRepository.findOne({"identifier": produkt.identifier});
    if(result) {
      result.details = produkt.details;
      await this.productsRepository.update(result.id, result);
    } else {
      result = this.productsRepository.create();
      result.identifier = produkt.identifier;
      result.details = produkt.details;
      await this.productsRepository.save(result);
    }
    return result;
  }
}
